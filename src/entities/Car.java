/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author samsung
 */
public abstract class Car {
int car_id,price,resale;
String model;
public Car(int car_id,String model,int price)
{
 this.car_id = car_id;
 this.model = model;
}
public int getcar_id()
{
return car_id; 
}


public void setcar_id(int car_id)
{
    this.car_id=car_id;
}

public String getmodel(){
return model;    
}

public void setmodel(String model)
{
    this.model=model;
}

public int getprice()
{
    return price;
}

public int setprice(int price){
 this.price=price;   
    return 0;
}

public int getresale()
{
    return resale;
}

    @Override
    public String toString() {
        return "model of car: "+this.model+"\nID of car: "+this.car_id+" \nPrice of car: "+this.price+" \nResale value of car is: \n"+this.resale;
    }

public void setresale_val(float resale_val)
{
    this.resale=resale;
}
}



