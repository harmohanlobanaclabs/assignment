/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;
import interfaces.Constants;
import static interfaces.Constants.ADD_CAR;
import static interfaces.Constants.ADD_CUSTOMER;
import static interfaces.Constants.CUSTOMER_DETAIL;
import static interfaces.Constants.EXIT;
import static interfaces.Constants.HYUNDAI;
import static interfaces.Constants.MARUTI;
import static interfaces.Constants.PRIZE;
import static interfaces.Constants.TOYOTA;
import static entities.Main_class.obj1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author samsung
 */
public class Main_class implements Constants{
     static int choice,m;
     static InputStreamReader obj=new InputStreamReader(System.in);
     static BufferedReader obj1=new BufferedReader(obj);
     static HashMap<Integer,Customer> obj3 = new HashMap<Integer,Customer>();
     static ArrayList<Car> a1 = new ArrayList<Car>();
     static ArrayList a2 = new ArrayList();  
     static ArrayList a3 = new ArrayList(); 
     ArrayList<Integer> a12 = new ArrayList<Integer>();
   public static void main(String[] args){
        // TODO code application logic here
        do{
        try {
            System.out.println("Please enter 1 to add Customer");
            System.out.println("Please press 2 to add Car");
            System.out.println("please press 3 to display ids and names of all the users");
            System.out.println("Please press 4 to display all the details of the customer");
            System.out.println("Please press 5 to know the lucky draw winner");
            System.out.println("Please press 6 to exit");
            choice=Integer.parseInt(obj1.readLine());
            {
            switch(choice)
            {
                case ADD_CUSTOMER:
                  add_customer();
                    
                    break;
                case ADD_CAR:
                   car_add();
                    break;
                    
                case CUSTOMER_NAME:
                     customer_name();
                    break;
                    
                case CUSTOMER_DETAIL:
                    customer_details();
                    break;
                    
                case PRIZE:
                    lucky_draw();
                    break;
                    
                case EXIT:
                    System.out.println("Thanks for using this service");
                    System.exit(0);      
                    break;
                    
                default:
                    System.out.println("PLEASE ENTER A VALID VALUE!"); 
            }
            }
        } catch (IOException ex) {
            Logger.getLogger(Main_class.class.getName()).log(Level.SEVERE, null, ex);
        }
          }while(choice!=6);
   }
       public static void add_customer() throws IOException
       {
        int id;
        String name;
        System.out.println("Please enter your id");
        id=Integer.parseInt(obj1.readLine());
        System.out.println("Please enter your name");
        name=obj1.readLine();
        Customer cust=new Customer(id,name); 
        if(obj3.get(id)==null){      
        obj3.put(id,cust);  
       } else{
            System.out.println("This id already exists,please enter another one");
        }
       }
       
       public static void customer_name()
       {
       for (Integer key : obj3.keySet()) {
       System.out.println("id: " + key );
       Customer value=obj3.get(key);
       System.out.println(" name: " +value.name);
         }
       } 
       
       public static void lucky_draw()
       {
       try {
           int id1,id2,id3;
           System.out.println("please enter 3 ids:");
           id1=Integer.parseInt(obj1.readLine());
           id2=Integer.parseInt(obj1.readLine());
           id3=Integer.parseInt(obj1.readLine());
           for (Integer key : obj3.keySet()) 
           {
           a3.add(key);
           }
           Collections.shuffle(a3);
           ArrayList<Integer> a12 = new ArrayList(a3.subList(0, 6));
           if(a12.contains(id1)){
              System.out.println("One of the winner is: "+id1);
           }
           if(a12.contains(id2)){
              System.out.println("One of the winner is: "+id2);
           }
           if(a12.contains(id3)){
              System.out.println("One of the winner is:"+id3);
           }
           }catch (IOException ex) {
           Logger.getLogger(Main_class.class.getName()).log(Level.SEVERE, null, ex);
           }
       }
       
       public static void customer_details() throws IOException
       {
         int get_id,key;
         System.out.println("Please enter your id");
         get_id=Integer.parseInt(obj1.readLine());
         if(obj3.get(get_id)!=null)
         {
           Customer value=obj3.get(get_id);
           System.out.println("Name of Customer: "+value.name);
           for(Car c:value.a1)
           {
             System.out.println(c);
           }
         }
         else
         {
           System.out.println("This id does not exists");
         }
        } 
       
        public static void car_add() throws IOException
        {
        int car_id,id,value,price;
        String model;
        System.out.println("please enter your id");
        id=Integer.parseInt(obj1.readLine());
        Customer cust=obj3.get(id);
        if(obj3.get(id)!=null){
        System.out.println("please enter the number corresponding to the car you want to select");
        System.out.println("Enter 1 for Hyundai,2 for Toyota,3 for maruti");
        value=Integer.parseInt(obj1.readLine());
        switch(value){
            case HYUNDAI:
                System.out.printf("You have selected HYUNDAI \n");
                System.out.println("Please enter the car ID");
                car_id=Integer.parseInt(obj1.readLine());
                System.out.println("Please enter the car model");
                model=obj1.readLine();
                System.out.println("Please enter the price of the car");
                price=Integer.parseInt(obj1.readLine());
                Car c1=new Hyundai(car_id,model,price);
                cust.a1.add(c1); 
                break;
            case TOYOTA:
                System.out.printf("You have selected TOYOTA \n");
                System.out.println("Please enter the car ID");
                car_id=Integer.parseInt(obj1.readLine());
                System.out.println("Please enter the car model");
                model=obj1.readLine();
                System.out.println("Please enter the price of the car");
                price=Integer.parseInt(obj1.readLine());
                Car c2=new Toyota(car_id,model,price);
                cust.a1.add(c2); 
                break;
            case MARUTI:
                System.out.printf("You have selected MARUTI \n");
                System.out.println("Please enter the car ID");
                car_id=Integer.parseInt(obj1.readLine());
                System.out.println("Please enter the car model");
                model=obj1.readLine();
                System.out.println("Please enter the price of the car");
                price=Integer.parseInt(obj1.readLine());
                Car c3=new Maruti(car_id,model,price);
                cust.a1.add(c3); 
                break;
            default:
                System.out.println("PLEASE ENTER A VALID ENTRY!");
          }
       }}

    private void add_user() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
        

